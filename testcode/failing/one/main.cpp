struct Result {
    double xout;
    bool valid;
};

Result function(double x, double y) {
    Result result;
    if (x > y) {
        result.valid = false;
        return result;
    }
    result.valid = true;
    result.xout = x;
    return result;
}
