#include <fixedpoint.hpp>

struct Input {
    Fp<5> x;
    Fp<5> y;
};

struct Result {
    Fp<5> __0;
    bool __1;
};

Result fp(Input input) {
    Result result;
    if (input.x > input.y) {
        result.__1 = false;
        return result;
    }
    result.__1 = true;
    result.__0 = 0;
    return result;
}
