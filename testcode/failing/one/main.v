module main(clk, rst, input_valid, output_valid, x_non_reg, y_non_reg, __0, __1);
    input clk;
    input rst;
    input input_valid;
    output output_valid;
    input[8:0] x_non_reg;
    input[8:0] y_non_reg;
    output reg[8:0] __0;
    output reg __1;

    always @(posedge clk) begin
        if (x_non_reg > 2) begin
            __1 <= 0;
        end
        else begin
            if ($signed(x_non_reg) < 0) begin
                __0 <= 0;
            end
            else if (x_non_reg > 1) begin
                __0 <= 1;
            end
            else begin
                __0 <= __0;
            end

            __1 <= 1;
        end
    end
endmodule
