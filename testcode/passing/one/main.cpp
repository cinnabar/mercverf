#include "util.hpp"

struct Result {
    double xout;
    bool valid;
};

Result function(double x, double y) {
    Result result;
    if (x > 2) {
        result.valid = false;
        return result;
    }
    result.valid = true;
    result.xout = clamp(x, 0, 1);
    return result;
}
