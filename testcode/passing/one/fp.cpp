#include <fixedpoint.hpp>

struct Input {
    Fp<5> x;
    Fp<5> y;
};

struct Result {
    Fp<5> __0;
    bool __1;
};

Fp<5> clamp(Fp<5> x, Fp<5> min, Fp<5> max) {
    if (x < min) {
        return min;
    }
    else if (x > max) {
        return max;
    }
    else {
        return x;
    }
}

Result fp(Input input) {
    Result result;
    if (input.x > input.y) {
        result.__1 = false;
        return result;
    }
    result.__1 = true;
    result.__0 = clamp(input.x, 0, 1);
    return result;
}
