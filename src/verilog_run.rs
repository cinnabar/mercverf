use std::process::Command;

use color_eyre::eyre::{self, anyhow, bail, Context, Result};
use log::info;
use nesty::{code, Code};
use regex::Regex;

use crate::{
    config::{Config, IoValues, OutputKind},
    output::{OutputValues, VerilogOutputValue, VerilogValue},
    run_util::parse_output,
};

fn generate_inputs(config: &Config, block: &[(String, f64)]) -> Vec<String> {
    block
        .iter()
        .map(|(name, value)| {
            let input_spec = config
                .io
                .inputs
                .iter()
                .filter(|spec| &spec.name == name)
                .next()
                .unwrap();

            let val = (value * (2i64).pow(input_spec.frac_bits as u32) as f64).floor() as i128;

            format!("_i_{} = 128'b{:b};", name, val)
        })
        .collect::<Vec<_>>()
}

fn generate_output_print(config: &Config, output_index: isize, display_internal: bool) -> Code {
    let display_code = config
        .io
        .outputs
        .iter()
        .enumerate()
        .map(|(i, o)| {
            if o.is_boolean() {
                format!("$display(\"{}: %0d\", _o_{});", o.name, o.name)
            } else {
                format!("$display(\"{}: %0d\", $signed(uut.__{}));", o.name, i)
            }
        })
        .collect::<Vec<_>>();

    let internal_displays = if display_internal {
        config
            .verilog
            .internal_values
            .iter()
            .map(|var| {
                format!(
                    "$display(\"(internal) {}: %0d\", $signed(uut.{}));",
                    var, var
                )
            })
            .collect()
    } else {
        vec![]
    };

    code! {
        [0] format!("output_idx <= {};", output_index);
        [0] format!(r#"$display("=={}==");"#, output_index);
        [0] display_code;
        [0] internal_displays;
    }
}

pub fn generate_code(
    config: &Config,
    wait_for_pipeline: bool,
    display_internal: bool,
) -> eyre::Result<Code> {
    let task = &config.verilog;

    let clk_gen = code! {
        [0] "initial begin";
        [1]     "clk = 0;";
        [1]     "forever begin";
        [2]         "#1;";
        [2]         "clk = ~clk;";
        [1]     "end";
        [0] "end"
    };

    let input_spec = code! {
        [0] "reg clk;";
        [0] config.io.inputs.iter()
            .map(|i| {
                let size = i.int_bits + i.frac_bits;
                // -1 + 1 because ranges are exclusive but we want to include
                // a sign bit
                format!("reg[{}:0] _i_{};", size-1+1, i.name)
            }).collect::<Vec<String>>();
    };
    let output_spec = config
        .io
        .outputs
        .iter()
        .map(|o| match o.kind {
            OutputKind::Boolean { early_ret: _ } => format!("wire _o_{};", o.name),
            OutputKind::Frac { max_error: _ } => format!("wire[127:0] _o_{};", o.name),
        })
        .collect::<Vec<_>>();
    let input_blocks = config.io.input_blocks();
    let main_block = if wait_for_pipeline {
        input_blocks
            .iter()
            .enumerate()
            .map(|(i, input)| {
                let input_code = generate_inputs(config, input);

                let delay_code = (0..task.pipeline_depth)
                    .map(|_| "`DELAY_NEGEDGE".to_string())
                    .collect::<Vec<_>>();

                let output_print = generate_output_print(config, i as isize, display_internal);

                code! {
                    [0] "`DELAY_NEGEDGE";
                    [0] input_code;
                    [0] delay_code;
                    [0] output_print;
                }
            })
            .collect()
    } else {
        if display_internal {
            bail!("Display internal can only be used with wait_for_pipeline");
        }
        (0..input_blocks.len() + task.pipeline_depth as usize)
            .map(|i| {
                let current_input = input_blocks.get(i);
                let output_index = i as isize - task.pipeline_depth as isize;

                let inputs = if let Some(block) = current_input {
                    generate_inputs(config, block)
                } else {
                    vec![]
                };

                let outputs = if output_index >= 0 {
                    generate_output_print(config, output_index, false)
                } else {
                    Code::new()
                };

                code! {
                    // [0] "@(negedge clk)";
                    [0] "`DELAY_NEGEDGE";
                    [0] inputs;
                    [0] outputs;
                }
            })
            .collect::<Vec<_>>()
    };

    let module_instance = {
        let inputs = config
            .io
            .inputs
            .iter()
            .map(|i| format!(", .{}_non_reg(_i_{})", i.name, i.name));
        let outputs = config
            .io
            .outputs
            .iter()
            .enumerate()
            .map(|(i, o)| format!(", .__{}(_o_{})", i, o.name));
        code! {
            [0] format!("{} uut", task.module);
            [1]     "( .clk(clk)";
            [1]     ", .rst(0)";
            [1]     ", .input_valid(1)";
            [1]     ", .output_valid(output_valid)";
            [1]     inputs.collect::<Vec<_>>();
            [1]     outputs.collect::<Vec<_>>();
            [1]     ");"
        }
    };

    Ok(code! {
        [0] format!("`include \"{}\"", task.main_file.to_string_lossy());
        [0] "`ifdef IS_VERILATOR";
        [0] "`define DELAY_NEGEDGE";
        [0] "`else";
        [0] "`define DELAY_NEGEDGE @(negedge clk)";
        [0] "`endif";
        [0] "module _test_runner_();";
        [1]     input_spec;
        [1]     output_spec;
        [1]     "wire output_valid;";
        [1]     "reg[31:0] output_idx;";
        [1]     "initial begin";
        [2]         format!("$dumpfile(\"{}.vcd\");", task.main_file.to_string_lossy());
        [2]         "$dumpvars(0, _test_runner_);";
        [1]     "end";
        [1]     clk_gen;
        [0]     "";
        [1]     "reg done;";
        [1]     "initial begin";
        [2]         "done = 0;";
        [2]         main_block;
        [2]         "done = 1;";
        [2]         "repeat(10) `DELAY_NEGEDGE;";
        [2]         "$finish();";
        [1]     "end";
        [0]     "";
        [1]     module_instance;
        [0] "endmodule";
    })
}

pub fn parse_verilog_output(
    output: &str,
    io: &IoValues,
) -> Result<OutputValues<VerilogOutputValue>> {
    let parser = |kind: &OutputKind, value_str: &str| -> Result<VerilogOutputValue> {
        match kind {
            OutputKind::Boolean { .. } => {
                let parsed = value_str
                    .parse::<VerilogValue<usize>>()
                    .with_context(|| format!("When parsing boolean number '{}'", value_str))?;
                Ok(VerilogOutputValue::Bool(parsed.map(|x| x == 1)))
            }
            OutputKind::Frac { .. } => {
                let parsed = value_str
                    .parse::<VerilogValue<i128>>()
                    .with_context(|| format!("When parsing fixedpoint value '{}'", value_str))?;
                Ok(VerilogOutputValue::F64(parsed))
            }
        }
    };

    parse_output(&output, &io, parser)
}

#[allow(dead_code)]
fn hl_line(re: &Regex, color_code: (i32, i32), line: &str) -> String {
    let replacement: &str = &format!("\x1b[{};{}m$0\x1b[0m", color_code.0, color_code.1);
    re.replace_all(&line, replacement).into()
}

#[allow(dead_code)]
fn color_verilator_output(output: &str) {
    for line in output.lines() {
        let mut result = String::from(line);
        result = hl_line(&Regex::new("Error").unwrap(), (0, 31), &result);
        result = hl_line(&Regex::new(r"\^~*").unwrap(), (0, 35), &result);
        result = hl_line(&Regex::new(r"Warning-?\w*").unwrap(), (1, 33), &result);
        result = hl_line(
            &Regex::new(r"([A-z_.0-9/]+)[A-z_.0-9]+\.[A-z0-9]:\d+:\d+").unwrap(),
            (0, 34),
            &result,
        );
        result = hl_line(&Regex::new(r"^\s*\.\.\..*").unwrap(), (0, 37), &result);
        println!("{}", result);
    }
}

#[allow(dead_code)]
fn color_iverilog_output(output: &str) {
    for line in output.lines() {
        let mut result = String::from(line);
        result = hl_line(&Regex::new("error:").unwrap(), (0, 31), &result);
        result = hl_line(&Regex::new(r"warning:").unwrap(), (1, 33), &result);
        result = hl_line(
            &Regex::new(r"^([._a-zA-Z0-9/]+):").unwrap(),
            (0, 37),
            &result,
        );
        println!("{}", result);
    }
}

/// Run the verilog task in `config`
///
/// If `wait_for_pipeline` is set, new values will not be inserted into the pipeline until
/// the previous computation is done. This can be helpful during debugging
pub fn run_verilog(
    config: &Config,
    wait_for_pipeline: bool,
    display_internal: bool,
) -> Result<OutputValues<VerilogOutputValue>> {
    let runner_code = generate_code(config, wait_for_pipeline, display_internal)?;
    let task = &config.verilog;

    std::fs::create_dir_all("mercverf_out").context(format!("Failed to create output dir"))?;
    let runner_verilog = format!("mercverf_out/_runner_{}.v", task.module);
    std::fs::write(&runner_verilog, runner_code.to_string())?;
    let runner_filename = format!("mercverf_out/runner_{}.vvp", task.module);

    info!("Runner verilog: {}", runner_verilog);
    info!("Runner filename: {}", runner_filename);

    info!("Linting verilog");
    let lint_result = Command::new("verilator")
        .arg("--lint-only")
        .arg("--relative-includes")
        .arg("-DIS_VERILATOR")
        .arg("-Wno-WIDTH")
        .arg("-Wno-STMTDLY")
        .arg("-Wno-STMTDLY")
        .arg("-Wno-INFINITELOOP")
        .arg(&runner_verilog)
        .args(&task.extra_files)
        .output()?;

    println!("stdout:");
    color_verilator_output(&String::from_utf8_lossy(&lint_result.stdout));
    println!("stderr:");
    color_verilator_output(&String::from_utf8_lossy(&lint_result.stderr));

    info!("Building verilog");
    let compile_result = Command::new("iverilog")
        .arg("-o")
        .arg(&runner_filename)
        .args(&task.extra_files)
        .arg("-grelative-include")
        .arg("-g2012")
        .arg(&runner_verilog)
        .arg("-s")
        .arg("_test_runner_")
        .output()?;

    // print!("{}", String::from_utf8_lossy(&compile_result.stderr));
    color_iverilog_output(&String::from_utf8_lossy(&compile_result.stderr));
    color_iverilog_output(&String::from_utf8_lossy(&compile_result.stdout));
    if !compile_result.status.success() {
        return Err(anyhow!("Compilation failed"));
    }

    println!("Running verilog");
    let run_result = Command::new("vvp")
        .arg(&runner_filename)
        .arg("-i")
        .arg("-v")
        .output()?;

    if !run_result.status.success() {
        println!("stdout:");
        print!("{}", String::from_utf8_lossy(&run_result.stdout));
        println!("stderr:");
        print!("{}", String::from_utf8_lossy(&run_result.stderr));
        println!("");
        return Err(anyhow!("Running compiled verilog failed"));
    }

    let output_str = String::from_utf8_lossy(&run_result.stdout);

    parse_verilog_output(&output_str, &config.io)
}

#[cfg(test)]
mod tests {
    use std::collections::HashMap;

    use super::*;
    use indoc::indoc;
    use nesty::assert_same_code;

    #[test]
    fn verilog_code_generation_works() {
        let config_toml = r#"
            [reference]
            function = "function"
            main_file = "main.cpp"
            extra_files = []

            [fixedpoint]
            function = ""
            main_file = ""
            extra_files = []

            [verilog]
            module = "under_test"
            main_file = "main.v"
            extra_files = []
            pipeline_depth = 2

            [io]
            inputs = [
                {name = "x1", int_bits = 5, frac_bits = 3, values = [0.5, 1.0]},
                {name = "x2", int_bits = 5, frac_bits = 3, values = [0.25, 1.5]},
            ]
            outputs = [
                { name = "o", kind = { v = "Frac", a = {max_error = 0.5 }}},
                { name = "valid", kind = { v = "Boolean", a = {early_ret = "InvalidWhenHigh"}}},
            ]
            "#;

        let config = Config::from_toml(config_toml).unwrap();

        let expected = indoc! {r#"
            `include "main.v"
            `ifdef IS_VERILATOR
            `define DELAY_NEGEDGE
            `else
            `define DELAY_NEGEDGE @(negedge clk)
            `endif
            module _test_runner_();
                reg clk;
                reg[8:0] _i_x1;
                reg[8:0] _i_x2;
                wire[127:0] _o_o;
                wire _o_valid;
                wire output_valid;
                reg[31:0] output_idx;
                initial begin
                    $dumpfile("main.v.vcd");
                    $dumpvars(0, _test_runner_);
                end
                initial begin
                    clk = 0;
                    forever begin
                        #1;
                        clk = ~clk;
                    end
                end

                reg done;
                initial begin
                    done = 0;
                    `DELAY_NEGEDGE
                    _i_x1 = 128'b100;
                    _i_x2 = 128'b10;
                    `DELAY_NEGEDGE
                    _i_x1 = 128'b1000;
                    _i_x2 = 128'b1100;
                    `DELAY_NEGEDGE
                    output_idx <= 0;
                    $display("==0==");
                    $display("o: %0d", $signed(_o_o));
                    $display("valid: %0d", _o_valid);
                    `DELAY_NEGEDGE
                    output_idx <= 1;
                    $display("==1==");
                    $display("o: %0d", $signed(_o_o));
                    $display("valid: %0d", _o_valid);
                    done = 1;
                    repeat(10) `DELAY_NEGEDGE;
                    $finish();
                end

                under_test uut
                    ( .clk(clk)
                    , .rst(0)
                    , .input_valid(1)
                    , .output_valid(output_valid)
                    , .x1_non_reg(_i_x1)
                    , .x2_non_reg(_i_x2)
                    , .__0(_o_o)
                    , .__1(_o_valid)
                    );
            endmodule"#
        };

        let result = generate_code(&config, false, false).unwrap();

        assert_same_code!(&result.to_string(), &expected);
    }

    #[test]
    fn verilog_code_generation_with_wait_for_pipeline_works() {
        let config_toml = r#"
            [reference]
            function = "function"
            main_file = "main.cpp"
            extra_files = []

            [fixedpoint]
            function = ""
            main_file = ""
            extra_files = []

            [verilog]
            module = "under_test"
            main_file = "main.v"
            extra_files = []
            pipeline_depth = 2

            [io]
            inputs = [
                {name = "x1", int_bits = 5, frac_bits = 3, values = [0.5, 1.0]},
                {name = "x2", int_bits = 5, frac_bits = 3, values = [0.25, 1.5]},
            ]
            outputs = [
                { name = "o", kind = { v = "Frac", a = {max_error = 0.5 }}},
                { name = "valid", kind = { v = "Boolean", a = {early_ret = "InvalidWhenHigh"}}},
            ]
            "#;

        let config = Config::from_toml(config_toml).unwrap();

        let expected = indoc! {r#"
            `include "main.v"
            `ifdef IS_VERILATOR
            `define DELAY_NEGEDGE
            `else
            `define DELAY_NEGEDGE @(negedge clk)
            `endif
            module _test_runner_();
                reg clk;
                reg[8:0] _i_x1;
                reg[8:0] _i_x2;
                wire[127:0] _o_o;
                wire _o_valid;
                wire output_valid;
                reg[31:0] output_idx;
                initial begin
                    $dumpfile("main.v.vcd");
                    $dumpvars(0, _test_runner_);
                end
                initial begin
                    clk = 0;
                    forever begin
                        #1;
                        clk = ~clk;
                    end
                end

                reg done;
                initial begin
                    done = 0;
                    `DELAY_NEGEDGE
                    _i_x1 = 128'b100;
                    _i_x2 = 128'b10;
                    `DELAY_NEGEDGE
                    `DELAY_NEGEDGE
                    output_idx <= 0;
                    $display("==0==");
                    $display("o: %0d", $signed(_o_o));
                    $display("valid: %0d", _o_valid);
                    `DELAY_NEGEDGE
                    _i_x1 = 128'b1000;
                    _i_x2 = 128'b1100;
                    `DELAY_NEGEDGE
                    `DELAY_NEGEDGE
                    output_idx <= 1;
                    $display("==1==");
                    $display("o: %0d", $signed(_o_o));
                    $display("valid: %0d", _o_valid);
                    done = 1;
                    repeat(10) `DELAY_NEGEDGE;
                    $finish();
                end

                under_test uut
                    ( .clk(clk)
                    , .rst(0)
                    , .input_valid(1)
                    , .output_valid(output_valid)
                    , .x1_non_reg(_i_x1)
                    , .x2_non_reg(_i_x2)
                    , .__0(_o_o)
                    , .__1(_o_valid)
                    );
            endmodule"#
        };

        let result = generate_code(&config, true, false).unwrap();

        assert_same_code!(&result.to_string(), &expected);
    }

    #[test]
    fn verilog_code_generation_with_wait_for_pipeline_and_display_internal_works() {
        let config_toml = r#"
            [reference]
            function = "function"
            main_file = "main.cpp"
            extra_files = []

            [fixedpoint]
            function = ""
            main_file = ""
            extra_files = []

            [verilog]
            module = "under_test"
            main_file = "main.v"
            extra_files = []
            pipeline_depth = 2
            internal_values = ["a"]

            [io]
            inputs = [
                {name = "x1", int_bits = 5, frac_bits = 3, values = [0.5]},
            ]
            outputs = [
                { name = "o", kind = { v = "Frac", a = {max_error = 0.5 }}},
            ]
            "#;

        let config = Config::from_toml(config_toml).unwrap();

        let expected = indoc! {r#"
            `include "main.v"
            `ifdef IS_VERILATOR
            `define DELAY_NEGEDGE
            `else
            `define DELAY_NEGEDGE @(negedge clk)
            `endif
            module _test_runner_();
                reg clk;
                reg[8:0] _i_x1;
                wire[127:0] _o_o;
                wire output_valid;
                reg[31:0] output_idx;
                initial begin
                    $dumpfile("main.v.vcd");
                    $dumpvars(0, _test_runner_);
                end
                initial begin
                    clk = 0;
                    forever begin
                        #1;
                        clk = ~clk;
                    end
                end

                reg done;
                initial begin
                    done = 0;
                    `DELAY_NEGEDGE
                    _i_x1 = 128'b100;
                    `DELAY_NEGEDGE
                    `DELAY_NEGEDGE
                    output_idx <= 0;
                    $display("==0==");
                    $display("o: %0d", $signed(_o_o));
                    $display("(internal) a: %0d", $signed(uut.a));
                    done = 1;
                    repeat(10) `DELAY_NEGEDGE;
                    $finish();
                end

                under_test uut
                    ( .clk(clk)
                    , .rst(0)
                    , .input_valid(1)
                    , .output_valid(output_valid)
                    , .x1_non_reg(_i_x1)
                    , .__0(_o_o)
                    );
            endmodule"#
        };

        let result = generate_code(&config, true, true).unwrap();

        assert_same_code!(&result.to_string(), &expected);
    }

    #[test]
    fn verilog_output_parsing_works() {
        let config_toml = r#"
            inputs = []
            outputs = [
                { name = "a", kind = { v = "Frac", a = {max_error = 0.5 } } },
                { name = "b", kind = { v = "Boolean", a = {early_ret = "InvalidWhenHigh"} } },
            ]
            "#;

        let io_values = IoValues::from_toml(&config_toml).unwrap();

        let input = indoc!(
            r#" ==0==
            a: 1
            b: 0
            ==1==
            a: 2
            b: 1"#,
        );

        let expected = OutputValues {
            results: vec![
                vec![
                    (
                        "a".to_string(),
                        VerilogOutputValue::F64(VerilogValue::Val(1)),
                    ),
                    (
                        "b".to_string(),
                        VerilogOutputValue::Bool(VerilogValue::Val(false)),
                    ),
                ]
                .into_iter()
                .collect(),
                vec![
                    (
                        "a".to_string(),
                        VerilogOutputValue::F64(VerilogValue::Val(2)),
                    ),
                    (
                        "b".to_string(),
                        VerilogOutputValue::Bool(VerilogValue::Val(true)),
                    ),
                ]
                .into_iter()
                .collect(),
            ],
            internal_values: vec![HashMap::new(), HashMap::new()],
        };

        assert_eq!(parse_verilog_output(&input, &io_values).unwrap(), expected);
    }

    #[test]
    fn verilog_output_parsing_with_internal_values_works() {
        let config_toml = r#"
            inputs = []
            outputs = [
                { name = "a", kind = { v = "Frac", a = {max_error = 0.5 } } },
            ]
            "#;

        let io_values = IoValues::from_toml(&config_toml).unwrap();

        let input = indoc!(
            r#" ==0==
            a: 1
            (internal) b: 5"#
        );

        let expected = OutputValues {
            results: vec![vec![(
                "a".to_string(),
                VerilogOutputValue::F64(VerilogValue::Val(1)),
            )]
            .into_iter()
            .collect()],
            internal_values: vec![vec![("b".to_string(), "5".to_string())]
                .into_iter()
                .collect()],
        };

        assert_eq!(parse_verilog_output(&input, &io_values).unwrap(), expected);
    }
}
