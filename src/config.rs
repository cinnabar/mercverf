use std::{collections::HashSet, path::PathBuf};

use serde::{Deserialize, Serialize};

#[derive(thiserror::Error, Debug, PartialEq, Clone)]
pub enum Error {
    #[error("Input {} has {} values, but the previous rows have {} values", .name, .got, .expected)]
    InputSequenceLenghtMissmatch {
        name: String,
        got: usize,
        expected: usize,
    },
    #[error("Found multiple definitions of input {0}")]
    DuplicateInput(String),
    #[error("Found multiple definitions of output {0}")]
    DuplicateOutput(String),
    #[error("Toml error {}", .0)]
    TomlError(#[from] toml::de::Error),
    #[error("Missing module name")]
    MissingModuleName,
}

pub type Result<T> = std::result::Result<T, Error>;

#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct Input {
    pub name: String,
    pub values: Vec<f64>,
    pub int_bits: usize,
    pub frac_bits: usize,
}

#[derive(Copy, Clone, Debug, PartialEq, Serialize, Deserialize)]
pub enum EarlyRetKind {
    InvalidWhenHigh,
    InvalidWhenLow,
}

#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
#[serde(tag = "v", content = "a")]
#[serde(deny_unknown_fields)]
pub enum OutputKind {
    Boolean { early_ret: Option<EarlyRetKind> },
    Frac { max_error: f64 },
}

#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct Output {
    pub name: String,
    pub kind: OutputKind,
}

impl Output {
    pub fn is_boolean(&self) -> bool {
        match self.kind {
            OutputKind::Boolean { .. } => true,
            OutputKind::Frac { .. } => false,
        }
    }
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct IoValues {
    pub inputs: Vec<Input>,
    pub outputs: Vec<Output>,
}

impl IoValues {
    #[cfg(test)]
    pub fn from_toml(toml_str: &str) -> Result<Self> {
        let parsed = toml::from_str::<Self>(toml_str)?;

        parsed.validate()?;

        Ok(parsed)
    }

    pub fn validate(&self) -> Result<()> {
        let expected_length = self.test_case_count();
        let mut seen_inputs = HashSet::new();

        self.inputs
            .iter()
            .map(|i| {
                if seen_inputs.contains(&i.name) {
                    return Err(Error::DuplicateInput(i.name.clone()));
                }
                seen_inputs.insert(i.name.clone());
                if i.values.len() != expected_length {
                    Err(Error::InputSequenceLenghtMissmatch {
                        name: i.name.clone(),
                        got: i.values.len(),
                        expected: expected_length,
                    })
                } else {
                    Ok(())
                }
            })
            .collect::<Result<Vec<_>>>()?;

        let mut seen_outputs = HashSet::new();
        self.outputs
            .iter()
            .map(|o| {
                if seen_outputs.contains(&o.name) {
                    return Err(Error::DuplicateOutput(o.name.clone()));
                }
                seen_outputs.insert(o.name.clone());
                Ok(())
            })
            .collect::<Result<_>>()?;

        Ok(())
    }

    pub fn input_blocks(&self) -> Vec<Vec<(String, f64)>> {
        (0..self.test_case_count())
            .map(|i| {
                let mut block = Vec::new();

                for input in &self.inputs {
                    block.push((input.name.clone(), input.values[i]));
                }

                block
            })
            .collect()
    }

    fn test_case_count(&self) -> usize {
        self.inputs
            .first()
            .map(|input| input.values.len())
            // If we have 0 inputs, we'll have to take the first output as reference
            .unwrap_or(0)
    }
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[serde(tag = "v", content = "a")]
#[serde(deny_unknown_fields)]
pub enum InputMethod {
    Positional,
    ByStruct(String),
}

impl Default for InputMethod {
    fn default() -> Self {
        Self::Positional
    }
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct CppTask {
    pub function: String,
    pub main_file: PathBuf,
    #[serde(default)]
    pub extra_files: Vec<PathBuf>,
    #[serde(default)]
    pub include_paths: Vec<String>,
    #[serde(default)]
    pub input_method: InputMethod,
    #[serde(default)]
    pub compiler_flags: Vec<String>,
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct VerilogTask {
    pub module: String,
    pub main_file: PathBuf,
    #[serde(default)]
    pub extra_files: Vec<PathBuf>,
    pub pipeline_depth: u32,
    /// Variables contained in the unit being tested
    #[serde(default)]
    pub internal_values: Vec<String>,
}

impl VerilogTask {
    pub fn validate(&self) -> Result<()> {
        if self.module.is_empty() {
            Err(Error::MissingModuleName)
        } else {
            Ok(())
        }
    }
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct Config {
    pub reference: CppTask,
    pub fixedpoint: CppTask,

    pub verilog: VerilogTask,

    pub io: IoValues,
}

impl Config {
    pub fn from_toml(toml_str: &str) -> Result<Self> {
        let parsed = toml::from_str::<Self>(toml_str)?;

        parsed.io.validate()?;
        parsed.verilog.validate()?;

        Ok(parsed)
    }
}

#[cfg(test)]
mod test {
    use super::*;

    use indoc::indoc;

    #[test]
    fn inputs_from_toml_works() {
        let input = r#"
        outputs = [
            { name = "u1", kind = { v = "Boolean", a = {early_ret = "InvalidWhenHigh"} } },
            { name = "u2", kind = { v = "Frac", a = {max_error = 0.5} } },
        ]
        inputs = [
            {name = "x1", int_bits = 5, frac_bits = 3, values = [0.5, 1.1, 2.2]},
            {name = "x2", int_bits = 6, frac_bits = 4, values = [-0.5, 1.1, -2.2]}
        ]
        "#;

        let result = IoValues::from_toml(input).unwrap();

        let expected = IoValues {
            inputs: vec![
                Input {
                    name: "x1".to_string(),
                    int_bits: 5,
                    frac_bits: 3,
                    values: vec![0.5, 1.1, 2.2],
                },
                Input {
                    name: "x2".to_string(),
                    int_bits: 6,
                    frac_bits: 4,
                    values: vec![-0.5, 1.1, -2.2],
                },
            ],
            outputs: vec![
                Output {
                    name: "u1".to_string(),
                    kind: OutputKind::Boolean {
                        early_ret: Some(EarlyRetKind::InvalidWhenHigh),
                    },
                },
                Output {
                    name: "u2".to_string(),
                    kind: OutputKind::Frac { max_error: 0.5 },
                },
            ],
        };

        assert_eq!(result, expected);
    }

    #[test]
    fn input_list_with_different_lengths_throws_error() {
        let input = r#"
        outputs = []
        inputs = [
            {name = "x1", int_bits = 5, frac_bits = 3, values = [0.5, 1.1]},
            {name = "x2", int_bits = 6, frac_bits = 4, values = [-0.5]}
        ]
        "#;

        let result = IoValues::from_toml(input);

        assert_eq!(
            result,
            Err(Error::InputSequenceLenghtMissmatch {
                name: "x2".to_string(),
                expected: 2,
                got: 1
            })
        );
    }

    #[test]
    fn input_block_generation_works() {
        let input = r#"
        inputs = [
            {name = "x1", int_bits = 5, frac_bits = 3, values = [0.5, 1.1]},
            {name = "x2", int_bits = 5, frac_bits = 3, values = [0.2, 1.3]},
        ]
        outputs = []
        "#;

        let result = IoValues::from_toml(input).unwrap().input_blocks();

        let expected = vec![
            vec![("x1".to_string(), 0.5), ("x2".to_string(), 0.2)],
            vec![("x1".to_string(), 1.1), ("x2".to_string(), 1.3)],
        ];

        assert_eq!(result, expected);
    }

    #[test]
    fn invalid_io_causes_invalid_config() {
        let input = indoc!(
            r#"
        [reference]
        function = "main"
        main_file = "main.cpp"
        extra_files = []

        [fixedpoint]
        function = "main"
        main_file = "fp.cpp"
        extra_files = []

        [verilog]
        module = "uut"
        main_file = "main.v"
        extra_files = []
        pipeline_depth = 1

        [io]
        inputs = [
            {name = "x1", int_bits = 5, frac_bits = 3, values = [0.5, 1.1]},
            {name = "x2", int_bits = 6, frac_bits = 4, values = [-0.5]}
        ]
        outputs = []
        "#
        );

        let result = Config::from_toml(input);

        assert_eq!(
            result,
            Err(Error::InputSequenceLenghtMissmatch {
                name: "x2".to_string(),
                expected: 2,
                got: 1
            })
        );
    }

    #[test]
    fn duplicate_input_specs_cause_error() {
        let input = indoc!(
            r#"
        inputs = [
            {name = "x1", int_bits = 5, frac_bits = 3, values = [0.5, 1.1]},
            {name = "x1", int_bits = 6, frac_bits = 4, values = [-0.5]}
        ]
        outputs = []
        "#
        );

        let result = IoValues::from_toml(input);

        assert_eq!(result, Err(Error::DuplicateInput("x1".to_string())));
    }

    #[test]
    fn duplicate_output_specs_cause_error() {
        let input = indoc!(
            r#"
        inputs = []
        outputs = [
            { name = "u1", kind = { v = "Boolean", a = {early_ret = "InvalidWhenHigh"} } },
            { name = "u1", kind = { v = "Frac", a = {max_error = 0.5} } },
        ]
        "#
        );

        let result = IoValues::from_toml(input);

        assert_eq!(result, Err(Error::DuplicateOutput("u1".to_string())));
    }
}
