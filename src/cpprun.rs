use std::process::Command;

use color_eyre::eyre::{anyhow, Context, Result};
use log::info;
use nesty::{code, Code};

use crate::config::Config;
use crate::config::{CppTask, InputMethod, IoValues, Output, OutputKind};
use crate::output::{FpOutputValue, OutputValues, RefOutputValue};
use crate::run_util::parse_output;

#[derive(Debug)]
pub struct CppFunction {
    pub name: String,
}

fn generate_reference_code(config: &Config) -> Code {
    let output_formater = |(_index, output): (usize, &Output)| {
        let value = match output.kind {
            OutputKind::Boolean { .. } => {
                format!("result.{}", output.name)
            }
            OutputKind::Frac { .. } => {
                format!("*reinterpret_cast<uint64_t*>(&result.{})", output.name)
            }
        };
        format!(
            r#"std::cout << "{}: " << {} << std::endl;"#,
            output.name, value
        )
    };
    generate_scaffold(&config.reference, &config.io, &output_formater)
}

fn generate_fixedpoint_code(config: &Config) -> Code {
    let output_formater = |(index, output): (usize, &Output)| {
        let struct_name = format!("__{}", index);
        match output.kind {
            OutputKind::Boolean { .. } =>
                format!(
                    r#"std::cout << "{}: " << result.{} << std::endl;"#,
                    output.name, struct_name
                ),
            OutputKind::Frac { .. } => code!(
                    // To get an address for the reinterpret cast, we need to store the result
                    // in a local variable first
                    [0] format!("double dp_raw_{} = result.{}.as_double();", output.name, struct_name);
                    [0] format!("uint64_t dp_{} = *reinterpret_cast<uint64_t*>(&dp_raw_{});", output.name, output.name);
                    [0] format!("auto raw_{} = result.{}._raw_value();", output.name, struct_name);
                    [0] format!(
                        r#"std::cout << "{}: " << dp_{} << ", " << raw_{} << std::endl;"#,
                        output.name,
                        output.name,
                        output.name
                    )
                )
                .to_string()
        }
    };

    generate_scaffold(&config.fixedpoint, &config.io, &output_formater)
}

fn generate_scaffold<F>(task: &CppTask, io: &IoValues, output_formater: &F) -> Code
where
    F: Fn((usize, &Output)) -> String,
{
    let array_defs = io
        .inputs
        .iter()
        .map(|input| {
            let values = input
                .values
                .iter()
                .map(|v| format!("{}", v))
                .collect::<Vec<_>>()
                .join(",\n");

            code! {
                [0] format!("double {}_values[] = {{", input.name);
                [1] values;
                [0] "};"
            }
        })
        .collect::<Vec<_>>();

    let executor = {
        let input = match &task.input_method {
            InputMethod::Positional => {
                let args = io
                    .inputs
                    .iter()
                    .map(|value| format!("{}_values[i]", value.name))
                    .collect::<Vec<_>>()
                    .join(", ");
                code! {[0] format!("auto result = {}({});", task.function, args);}
            }
            InputMethod::ByStruct(name) => {
                let assignments = io
                    .inputs
                    .iter()
                    .map(|value| format!("input.{} = {}_values[i];", value.name, value.name))
                    .collect::<Vec<_>>();
                code! {
                    [0] format!("{} input;", name);
                    [0] assignments;
                    [0] format!("auto result = {}(input);", task.function);
                }
            }
        };

        let output_print = io
            .outputs
            .iter()
            .enumerate()
            .map(output_formater)
            .collect::<Vec<_>>();

        code! {
            [0] &input;
            [0] &format!(r#"std::cout << "==" << i << "==" << std::endl;"#);
            [0] &output_print;
        }
    };

    let main_file_path = if task.main_file.is_absolute() {
        format!("{}", task.main_file.to_string_lossy())
    } else {
        format!("../{}", task.main_file.to_string_lossy())
    };

    code! {
        [0] &"#include <iostream>";
        [0] &"#include <cstdint>";
        // Including a cpp file is an ugly way of doing things, but we need
        // to build both anyway, so it should be fine, and this saves
        // us from requiring a header file with function defitions
        [0] &format!("#include \"{}\"", main_file_path);
        [0] array_defs;
        [0] &"int main() {";
        [1]     format!("for(std::size_t i = 0; i < {}; i++) {{", io.input_blocks().len());
        [2]         &executor;
        [1]     "}";
        [0] &"}"
    }
}

fn parse_reference_output(output_str: &str, io: &IoValues) -> Result<OutputValues<RefOutputValue>> {
    let parser = |kind: &OutputKind, value_str: &str| -> Result<RefOutputValue> {
        match kind {
            OutputKind::Boolean { .. } => {
                Ok(RefOutputValue::Bool(value_str.parse::<usize>()? == 1))
            }
            OutputKind::Frac { .. } => Ok(RefOutputValue::F64(f64::from_bits(value_str.parse()?))),
        }
    };

    parse_output(&output_str, &io, parser)
}

fn parse_fixedpoint_output(output_str: &str, io: &IoValues) -> Result<OutputValues<FpOutputValue>> {
    let parser = |kind: &OutputKind, value_str: &str| -> Result<FpOutputValue> {
        match kind {
            OutputKind::Boolean { .. } => Ok(FpOutputValue::Bool(value_str.parse::<usize>()? == 1)),
            OutputKind::Frac { .. } => {
                let comma_separated = value_str.split(",").collect::<Vec<_>>();
                if comma_separated.len() != 2 {
                    return Err(anyhow!(
                        "Expected 'float, raw fixedpoint', found '{}'",
                        value_str
                    ));
                }

                let float_part = comma_separated[0].trim();
                let fp_part = comma_separated[1].trim();
                Ok(FpOutputValue::F64((
                    f64::from_bits(float_part.parse()?),
                    fp_part
                        .parse::<i128>()
                        .context(format!("In '{}', When parsing fixedpoint bytes", fp_part))?,
                )))
            }
        }
    };

    parse_output(&output_str, &io, parser)
}

pub fn run_cpp<O, F>(
    run_type: &str,
    runner_code: Code,
    task: &CppTask,
    io: &IoValues,
    result_parser: F,
    use_debugger: bool,
) -> Result<OutputValues<O>>
where
    F: Fn(&str, &IoValues) -> Result<OutputValues<O>>,
{
    std::fs::create_dir_all("mercverf_out").context(format!("Failed to create output dir"))?;
    let runner_cpp_filename = format!("mercverf_out/_runner_{}_{}.cpp", run_type, task.function);
    std::fs::write(&runner_cpp_filename, runner_code.to_string())?;

    let runner_filename = format!("mercverf_out/_runner_{}_{}", run_type, task.function);

    let opt_args = if use_debugger {
        vec!["-O0", "-g"]
    } else {
        vec!["-O3"]
    };

    info!("Compiling");
    let compile_result = Command::new("clang++")
        .arg("-std=c++20")
        .arg("-fcolor-diagnostics")
        .args(&task.compiler_flags)
        .args(opt_args)
        .args(&["-o", &runner_filename])
        .arg(runner_cpp_filename)
        .args(&task.extra_files)
        .args(
            &task
                .include_paths
                .iter()
                .map(|p| format!("-I{}", p))
                .collect::<Vec<_>>(),
        )
        .output()?;

    print!("{}", String::from_utf8_lossy(&compile_result.stderr));
    if !compile_result.status.success() {
        return Err(anyhow!("Compilation failed"));
    }

    info!("Running");
    if use_debugger {
        Command::new("gdb")
            .arg(format!("./{}", runner_filename))
            .output()?;
    }
    let run_result = Command::new(format!("./{}", runner_filename)).output()?;

    if !run_result.status.success() {
        println!("stdout:");
        print!("{}", String::from_utf8_lossy(&run_result.stdout));
        println!("stderr:");
        print!("{}", String::from_utf8_lossy(&run_result.stderr));
        println!("");
        return Err(anyhow!("Running compiled C++ failed"));
    }

    let output_str = String::from_utf8_lossy(&run_result.stdout);

    println!("{}", output_str);

    result_parser(&output_str, &io)
}

/// Run a reference C++ model, returning the output
pub fn run_reference(config: &Config, use_debugger: bool) -> Result<OutputValues<RefOutputValue>> {
    let runner_code = generate_reference_code(config);
    run_cpp(
        "refrence",
        runner_code,
        &config.reference,
        &config.io,
        parse_reference_output,
        use_debugger,
    )
}

pub fn run_fixedpoint(config: &Config, use_debugger: bool) -> Result<OutputValues<FpOutputValue>> {
    let runner_code = generate_fixedpoint_code(config);

    run_cpp(
        "fixedpoint",
        runner_code,
        &config.fixedpoint,
        &config.io,
        parse_fixedpoint_output,
        use_debugger,
    )
}

#[cfg(test)]
mod tests {
    use std::collections::HashMap;

    use super::*;
    use indoc::{formatdoc, indoc};
    use nesty::assert_same_code;

    #[test]
    fn reference_codegen_works() {
        let config_toml = r#"
            [reference]
            function = "function"
            main_file = "main.cpp"
            extra_files = []

            [fixedpoint]
            function = ""
            main_file = ""
            extra_files = []

            [verilog]
            module = "main"
            main_file = ""
            extra_files = []
            pipeline_depth = 1

            [io]
            inputs = [
                {name = "x1", int_bits = 5, frac_bits = 3, values = [0.5, 1.1]},
                {name = "x2", int_bits = 5, frac_bits = 3, values = [0.2, 1.3]},
            ]
            outputs = [
                { name = "o", kind = { v = "Frac", a = {max_error = 0.5 }}},
                { name = "valid", kind = { v = "Boolean", a = {early_ret = "InvalidWhenHigh"}}},
            ]
            "#;

        let config = Config::from_toml(config_toml).unwrap();

        let expected = indoc! {r#"
            #include <iostream>
            #include <cstdint>
            #include "../main.cpp"
            double x1_values[] = {
                0.5,
                1.1
            };
            double x2_values[] = {
                0.2,
                1.3
            };
            int main() {
                for(std::size_t i = 0; i < 2; i++) {
                    auto result = function(x1_values[i], x2_values[i]);
                    std::cout << "==" << i << "==" << std::endl;
                    std::cout << "o: " << *reinterpret_cast<uint64_t*>(&result.o) << std::endl;
                    std::cout << "valid: " << result.valid << std::endl;
                }
            }"#
        };

        let result = generate_reference_code(&config);

        assert_same_code!(&result.to_string(), &expected);
    }

    #[test]
    fn fixedpoint_codegen_works() {
        let config_toml = r#"
            [reference]
            function = "function"
            main_file = "main.cpp"

            [fixedpoint]
            function = "fpfn"
            main_file = "fp.cpp"
            input_method = {v = "ByStruct", a = "InputStruct"}

            [verilog]
            module = "main"
            main_file = ""
            pipeline_depth = 1

            [io]
            inputs = [
                {name = "x1", int_bits = 5, frac_bits = 3, values = [0.5, 1.1]},
                {name = "x2", int_bits = 5, frac_bits = 3, values = [0.2, 1.3]},
            ]
            outputs = [
                { name = "o", kind = { v = "Frac", a = {max_error = 0.5 }}},
                { name = "valid", kind = { v = "Boolean", a = {early_ret = "InvalidWhenHigh"}}},
            ]
            "#;

        let config = Config::from_toml(config_toml).unwrap();

        let expected = indoc! {r#"
            #include <iostream>
            #include <cstdint>
            #include "../fp.cpp"
            double x1_values[] = {
                0.5,
                1.1
            };
            double x2_values[] = {
                0.2,
                1.3
            };
            int main() {
                for(std::size_t i = 0; i < 2; i++) {
                    InputStruct input;
                    input.x1 = x1_values[i];
                    input.x2 = x2_values[i];
                    auto result = fpfn(input);
                    std::cout << "==" << i << "==" << std::endl;
                    double dp_raw_o = result.__0.as_double();
                    uint64_t dp_o = *reinterpret_cast<uint64_t*>(&dp_raw_o);
                    auto raw_o = result.__0._raw_value();
                    std::cout << "o: " << dp_o << ", " << raw_o << std::endl;
                    std::cout << "valid: " << result.__1 << std::endl;
                }
            }"#
        };

        let result = generate_fixedpoint_code(&config);

        assert_same_code!(&result.to_string(), &expected);
    }

    #[test]
    fn absolute_paths_for_files_work_codegen_works() {
        let config_toml = r#"
            [reference]
            function = "function"
            main_file = "/home/x/y/main.cpp"
            extra_files = []

            [fixedpoint]
            function = ""
            main_file = ""
            extra_files = []

            [verilog]
            module = "main"
            main_file = ""
            extra_files = []
            pipeline_depth = 1

            [io]
            inputs = [
                {name = "x1", int_bits = 5, frac_bits = 3, values = [0.5, 1.1]},
                {name = "x2", int_bits = 5, frac_bits = 3, values = [0.2, 1.3]},
            ]
            outputs = [
                { name = "o", kind = { v = "Frac", a = {max_error = 0.5 }}},
                { name = "valid", kind = { v = "Boolean", a = {early_ret = "InvalidWhenHigh"}}},
            ]
            "#;

        let config = Config::from_toml(config_toml).unwrap();

        let expected = indoc! {r#"
            #include <iostream>
            #include <cstdint>
            #include "/home/x/y/main.cpp"
            double x1_values[] = {
                0.5,
                1.1
            };
            double x2_values[] = {
                0.2,
                1.3
            };
            int main() {
                for(std::size_t i = 0; i < 2; i++) {
                    auto result = function(x1_values[i], x2_values[i]);
                    std::cout << "==" << i << "==" << std::endl;
                    std::cout << "o: " << *reinterpret_cast<uint64_t*>(&result.o) << std::endl;
                    std::cout << "valid: " << result.valid << std::endl;
                }
            }"#
        };

        let result = generate_reference_code(&config);

        assert_same_code!(&result.to_string(), &expected);
    }

    #[test]
    fn reference_output_parsing_works() {
        let config_toml = r#"
            inputs = []
            outputs = [
                { name = "a", kind = { v = "Frac", a = {max_error = 0.5 } } },
                { name = "b", kind = { v = "Boolean", a = {early_ret = "InvalidWhenHigh"} } },
            ]
            "#;

        let io_values = IoValues::from_toml(&config_toml).unwrap();

        let input = formatdoc!(
            r#" ==0==
            a: {}
            b: 0
            ==1==
            a: 0
            b: 1"#,
            u64::from_ne_bytes((1f64).to_ne_bytes())
        );

        let expected = OutputValues {
            results: vec![
                vec![
                    ("a".to_string(), RefOutputValue::F64(1.)),
                    ("b".to_string(), RefOutputValue::Bool(false)),
                ]
                .into_iter()
                .collect(),
                vec![
                    ("a".to_string(), RefOutputValue::F64(0.)),
                    ("b".to_string(), RefOutputValue::Bool(true)),
                ]
                .into_iter()
                .collect(),
            ],
            internal_values: vec![HashMap::new(), HashMap::new()],
        };

        assert_eq!(
            parse_reference_output(&input, &io_values).unwrap(),
            expected
        );
    }

    #[test]
    fn fixedpoint_output_parsing_works() {
        let config_toml = r#"
            inputs = []
            outputs = [
                { name = "a", kind = { v = "Frac", a = {max_error = 0.5 } } },
                { name = "b", kind = { v = "Boolean", a = {early_ret = "InvalidWhenHigh"} } },
            ]
            "#;

        let io_values = IoValues::from_toml(&config_toml).unwrap();

        let input = formatdoc!(
            r#" ==0==
            a: {}, 1
            b: 0
            ==1==
            a: 0, 2
            b: 1"#,
            u64::from_ne_bytes((1f64).to_ne_bytes())
        );

        let expected = OutputValues {
            results: vec![
                vec![
                    ("a".to_string(), FpOutputValue::F64((1., 1))),
                    ("b".to_string(), FpOutputValue::Bool(false)),
                ]
                .into_iter()
                .collect(),
                vec![
                    ("a".to_string(), FpOutputValue::F64((0., 2))),
                    ("b".to_string(), FpOutputValue::Bool(true)),
                ]
                .into_iter()
                .collect(),
            ],
            internal_values: vec![HashMap::new(), HashMap::new()],
        };

        assert_eq!(
            parse_fixedpoint_output(&input, &io_values).unwrap(),
            expected
        );
    }
}
