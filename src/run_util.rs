use std::collections::HashMap;

use color_eyre::eyre::{anyhow, bail, Result};
use regex::Regex;

use crate::{
    config::{IoValues, OutputKind},
    output::OutputValues,
};

pub fn parse_output<O, F>(
    output_str: &str,
    io: &IoValues,
    value_parser: F,
) -> Result<OutputValues<O>>
where
    F: Fn(&OutputKind, &str) -> Result<O>,
{
    let mut result = OutputValues {
        results: vec![],
        internal_values: vec![],
    };

    let dumpfile_regex = Regex::new("VCD info: dumpfile").unwrap();
    let new_batch_re = Regex::new(r#"==(\d+)=="#).unwrap();
    let value_regex = Regex::new(r#"^(\w+): (.*)$"#).unwrap();
    let internal_regex = Regex::new(r"^\(internal\) (\w+): (.*)$").unwrap();
    let expected_values = io
        .outputs
        .iter()
        .map(|o| (o.name.clone(), o.kind.clone()))
        .collect::<HashMap<_, _>>();
    for line in output_str.lines() {
        if let Some(cap) = new_batch_re.captures_iter(line).next() {
            let batch_index = cap[1].parse::<usize>()?;
            assert!(batch_index == result.results.len());
            result.results.push(HashMap::new());
            result.internal_values.push(HashMap::new());
        } else if let Some(cap) = value_regex.captures_iter(line).next() {
            let var_name = cap[1].to_string();
            let value_str = cap[2].to_string();

            let output_kind = expected_values.get(&var_name).ok_or(anyhow!(
                "Did not find a value corresponding to {:?}",
                var_name
            ))?;

            let value = value_parser(&output_kind, &value_str)?;

            result.results.last_mut().unwrap().insert(var_name, value);
        } else if line.is_empty() {
        } else if dumpfile_regex.is_match(line) {
        } else if let Some(cap) = internal_regex.captures_iter(line).next() {
            let var_name = cap[1].to_string();
            let value_str = cap[2].to_string();

            result
                .internal_values
                .last_mut()
                .unwrap()
                .insert(var_name, value_str);
        } else {
            bail!("Unexpected output: '{}'", line)
        }
    }

    Ok(result)
}
