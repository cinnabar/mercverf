use std::path::{Path, PathBuf};

use color_eyre::eyre::{self, bail};
use color_eyre::eyre::{anyhow, Context};
use config::Config;
use crossbeam_utils::thread;
use log::info;
use output::{FpOutputValue, OutputValues, RefOutputValue, VerilogOutputValue};
use simplelog::{ColorChoice, ConfigBuilder, LevelFilter, TermLogger, TerminalMode};
use structopt::StructOpt;

mod comparison;
mod config;
mod cpprun;
mod output;
mod run_util;
mod tempcd;
#[cfg(test)]
mod testcode;
mod verilog_run;

use crate::comparison::{check_fixedpoint, check_verilog, count_valid};

#[derive(StructOpt)]
struct Opt {
    #[structopt()]
    /// Directory containing the test toml or json file
    test_dir: PathBuf,

    /// Run a specific input instead of all of them
    #[structopt(short, long)]
    input_num: Option<usize>,

    /// If set, the runner verilog will wait for an input to fully propagate through
    /// the pipeline before feeding the next input. This might aid in debugging but
    /// will hide errors in the pipelining
    #[structopt(long)]
    wait_for_pipeline: bool,

    /// Display all internal variables in the unit being tested. Must be combined
    /// with `wait-for-pipeline`
    #[structopt(long)]
    display_internal: bool,

    #[structopt(short, long)]
    debugger: bool,
}

pub struct RunOutput {
    pub config: Config,
    pub reference: OutputValues<RefOutputValue>,
    pub fixedpoint: OutputValues<FpOutputValue>,
    pub verilog: OutputValues<VerilogOutputValue>,
}

fn read_config(
    dir: &Path,
    config_file: &Path,
    decoder: impl Fn(&str) -> eyre::Result<Config>,
) -> eyre::Result<Config> {
    let config_path = dir.join(config_file);
    let config_str = std::fs::read_to_string(&config_file)
        .with_context(|| format!("Failed to read {}", config_path.to_string_lossy()))?;

    let config =
        decoder(&config_str).with_context(|| format!("In {}", config_path.to_string_lossy()))?;

    Ok(config)
}

pub struct RunModelsArgs<'a> {
    test_dir: &'a Path,
    use_debugger: bool,
    wait_for_pipeline: bool,
    display_internal: bool,
    input_num: Option<usize>,
}

fn run_models<'a>(args: RunModelsArgs<'a>) -> eyre::Result<RunOutput> {
    let dir = args.test_dir;
    if !dir.exists() {
        return Err(anyhow!("'{}': No such directory", dir.to_string_lossy()));
    }

    let _cd_token =
        tempcd::cd(&dir).with_context(|| format!("Failed to cd into {}", dir.to_string_lossy()))?;

    let toml_config = PathBuf::from("mercverf.toml");
    let json_config = PathBuf::from("mercverf.json");

    let mut config = if toml_config.exists() {
        read_config(&dir, &toml_config, |s| {
            toml::from_str(s).context("failed to decode toml")
        })?
    } else if json_config.exists() {
        read_config(&dir, &json_config, |s| {
            serde_json::from_str(s).context("failed to decode json")
        })?
    } else {
        bail!(
            "Did not find {} or {} in {}",
            toml_config.to_string_lossy(),
            json_config.to_string_lossy(),
            dir.to_string_lossy()
        )
    };

    if let Some(input_num) = args.input_num {
        config.io.inputs = config
            .io
            .inputs
            .into_iter()
            .map(|mut x| {
                x.values = x
                    .values
                    .into_iter()
                    .enumerate()
                    .filter_map(|(i, x)| if i == input_num { Some(x) } else { None })
                    .collect();
                x
            })
            .collect();
    }

    let (reference, fixedpoint, verilog) = thread::scope(|s| {
        let ref_thread = s.spawn(|_| {
            info!("Running reference");
            cpprun::run_reference(&config, args.use_debugger)
        });
        let reference = ref_thread.join().unwrap();
        let fp_thread = s.spawn(|_| {
            info!("Running fixedpoint");
            cpprun::run_fixedpoint(&config, args.use_debugger)
        });
        let fixedpoint = fp_thread.join().unwrap();
        let verilog_thread = s.spawn(|_| {
            info!("Running verilog");
            verilog_run::run_verilog(&config, args.wait_for_pipeline, args.display_internal)
        });

        let verilog = verilog_thread.join().unwrap();

        (reference, fixedpoint, verilog)
    })
    .unwrap();

    let reference = reference?;
    let fixedpoint = fixedpoint?;
    let verilog = verilog?;

    let internal_dump = verilog
        .internal_values
        .iter()
        .enumerate()
        .map(|(i, value_group)| {
            let values = value_group
                .iter()
                .map(|(name, value)| format!("{}: {}", name, value))
                .collect::<Vec<_>>()
                .join("\n");

            format!("=={}==\n{}", i, values)
        })
        .collect::<Vec<_>>()
        .join("\n");

    let internal_values_file = "mercverf_out/verilog_internal_values.txt";
    std::fs::write(&internal_values_file, internal_dump).with_context(|| {
        format!(
            "Failed to write internal verilog values to {}",
            internal_values_file
        )
    })?;

    println!("Reference valid: {}", count_valid(&config.io, &reference));
    println!("Fixedpoint valid: {}", count_valid(&config.io, &fixedpoint));
    println!("Verilog valid: {}", count_valid(&config.io, &verilog));

    Ok(RunOutput {
        config,
        reference,
        fixedpoint,
        verilog,
    })
}

/// Compare the fixedpoint to the reference, printing a report and returning Ok or Err
/// depending on the result.
pub fn run_fixedpoint_checks(output: &RunOutput) -> Result<(), ()> {
    comparison::run_checks(
        &output.config.io,
        "C++ fixedpoint",
        || check_fixedpoint(&output.config.io, &output.reference, &output.fixedpoint),
        |b| format!("{}", b),
        |b| format!("{}", b),
        |f| format!("{}", f),
        |fi| format!("{}", fi.0),
    )
}

pub fn run_verilog_checks(output: &RunOutput) -> Result<(), ()> {
    println!("Verilog result count {}", output.verilog.results.len());
    comparison::run_checks(
        &output.config.io,
        "Verilog",
        || check_verilog(&output.config.io, &output.fixedpoint, &output.verilog),
        |b| format!("{}", b),
        |b| format!("{}", b),
        |fi| format!("{}", fi.1),
        |vf| format!("{}", vf),
    )
}

fn main() -> color_eyre::eyre::Result<()> {
    color_eyre::install()?;
    let opt = Opt::from_args();

    let mut log_config = ConfigBuilder::new();
    log_config
        .add_filter_ignore(format!("llvm_ir"))
        .set_time_level(LevelFilter::Off);

    TermLogger::init(
        LevelFilter::Trace,
        log_config.build(),
        TerminalMode::Mixed,
        ColorChoice::Always,
    )
    .expect("Failed to initialise logger");

    let output = run_models(RunModelsArgs {
        test_dir: &opt.test_dir,
        use_debugger: opt.debugger,
        wait_for_pipeline: opt.wait_for_pipeline,
        display_internal: opt.display_internal,
        input_num: opt.input_num,
    })?;

    let mut output_missmatch = false;
    if run_fixedpoint_checks(&output).is_err() {
        output_missmatch = true;
    }
    if run_verilog_checks(&output).is_err() {
        output_missmatch = true;
    }

    if output_missmatch {
        bail!("Output missmatch")
    }
    Ok(())
}
