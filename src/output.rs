use std::collections::HashMap;

use thiserror::Error;

#[derive(Error, Debug, Clone)]
pub enum ParseError<E: 'static + std::fmt::Debug + std::error::Error> {
    #[error("Invalid character in string")]
    InvalidCharacter,
    #[error("Error parsing internal value")]
    ValueParseError(#[from] E),
}

#[derive(Copy, Clone, Debug, PartialEq)]
pub enum VerilogValue<T> {
    /// A verilog value which contains at least one X or Z but also contains
    /// another character, for example `0X`, `XZ01` etc.
    Impure,
    X,
    Z,
    Val(T),
}

impl<T> VerilogValue<T> {
    pub fn map<U, F>(self, f: F) -> VerilogValue<U>
    where
        F: Fn(T) -> U,
    {
        match self {
            VerilogValue::Impure => VerilogValue::Impure,
            VerilogValue::X => VerilogValue::X,
            VerilogValue::Z => VerilogValue::Z,
            VerilogValue::Val(val) => VerilogValue::Val(f(val)),
        }
    }
}

impl<T> std::str::FromStr for VerilogValue<T>
where
    T: std::str::FromStr,
    T::Err: 'static + std::fmt::Debug + std::error::Error,
{
    type Err = ParseError<T::Err>;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let s = s.to_ascii_uppercase();
        let has_unexpected_chars = !s.chars().all(|c| c.is_digit(10) || c == 'X' || c == 'Z' || c == '-');

        if has_unexpected_chars {
            Err(ParseError::InvalidCharacter)
        } else {
            let has_x = s.contains(|c| c == 'X');
            let has_z = s.contains(|c| c == 'Z');
            let has_digits = s.contains(|c: char| c.is_digit(10));

            Ok(match (has_x, has_z, has_digits) {
                (true, true, _) => Self::Impure,
                (true, _, true) => Self::Impure,
                (_, true, true) => Self::Impure,
                (true, false, false) => Self::X,
                (false, true, false) => Self::Z,
                (false, false, _) => Self::Val(s.parse()?),
            })
        }
    }
}

impl<T> PartialEq<T> for VerilogValue<T>
where
    T: PartialEq<T>,
{
    fn eq(&self, other: &T) -> bool {
        match self {
            VerilogValue::Impure => false,
            VerilogValue::X => false,
            VerilogValue::Z => false,
            VerilogValue::Val(v) if v == other => true,
            VerilogValue::Val(_) => false,
        }
    }
}

impl<T> std::fmt::Display for VerilogValue<T>
where
    T: std::fmt::Display,
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            VerilogValue::Impure => write!(f, "impure"),
            VerilogValue::X => write!(f, "X"),
            VerilogValue::Z => write!(f, "Z"),
            VerilogValue::Val(v) => write!(f, "{}", v),
        }
    }
}

macro_rules! impl_assume {
    ($target:ident, $bool_type:ty, $f64_type:ty) => {
        impl OutputValue for $target {
            type F = $f64_type;
            type B = $bool_type;

            fn assume_f64(&self) -> Self::F {
                match self {
                    Self::Bool(_) => panic!("Got a bool value when expecting a f64"),
                    Self::F64(val) => *val,
                }
            }
            fn assume_bool(&self) -> Self::B {
                match self {
                    Self::Bool(val) => *val,
                    Self::F64(_) => panic!("Got a f64 value when expecting a bool"),
                }
            }
        }
    };
}

pub trait OutputValue {
    type F;
    type B;
    fn assume_f64(&self) -> Self::F;
    fn assume_bool(&self) -> Self::B;
}

#[derive(Debug, PartialEq)]
pub enum RefOutputValue {
    Bool(bool),
    F64(f64),
}
impl_assume!(RefOutputValue, bool, f64);

#[derive(Debug, PartialEq)]
pub enum FpOutputValue {
    Bool(bool),
    F64((f64, i128)),
}

impl_assume!(FpOutputValue, bool, (f64, i128));

#[derive(Debug, PartialEq)]
pub enum VerilogOutputValue {
    Bool(VerilogValue<bool>),
    F64(VerilogValue<i128>),
}
impl_assume!(VerilogOutputValue, VerilogValue<bool>, VerilogValue<i128>);

#[derive(Debug, PartialEq)]
pub struct OutputValues<O> {
    pub results: Vec<HashMap<String, O>>,
    pub internal_values: Vec<HashMap<String, String>>,
}
