use std::path::{Path, PathBuf};

#[must_use]
pub struct TemporaryCd {
    original_dir: PathBuf,
}

pub fn cd(target: &Path) -> std::io::Result<TemporaryCd> {
    let original_dir = std::env::current_dir()?;
    std::env::set_current_dir(target)?;
    Ok(TemporaryCd { original_dir })
}

impl Drop for TemporaryCd {
    fn drop(&mut self) {
        std::env::set_current_dir(&self.original_dir).expect("failed to restore working directory")
    }
}
