use colored::Colorize;
use log::info;
use thiserror::Error;

use crate::{
    config::{EarlyRetKind, IoValues, OutputKind},
    output::{
        FpOutputValue, OutputValue, OutputValues, RefOutputValue, VerilogOutputValue, VerilogValue,
    },
};

#[derive(Error, Debug, PartialEq, Clone)]
pub enum Error<B1, B2, F1, F2>
where
    B1: std::fmt::Debug + Clone,
    B2: std::fmt::Debug + Clone,
    F1: std::fmt::Debug + Clone,
    F2: std::fmt::Debug + Clone,
{
    #[error("Found {reference} reference value but {fixedpoint} values to check")]
    ArrayLengthMissmatch { reference: usize, fixedpoint: usize },
    #[error(
        "{variable} is incorrect. Found {got:?} expected {expected:?} in {input_num}:th input"
    )]
    BoolMissmatch {
        expected: B1,
        got: B2,
        variable: String,
        input_num: usize,
    },
    #[error(
        "{variable} is incorrect. Found {got:?} expected {expected:?} in {input_num}:th input"
    )]
    F64Missmatch {
        expected: F1,
        got: F2,
        variable: String,
        input_num: usize,
    },
}

pub fn check_values<O1, O2, BoolCmp, F64Cmp>(
    io: &IoValues,
    lhs: &OutputValues<O1>,
    rhs: &OutputValues<O2>,
    bool_cmp: BoolCmp,
    f64_cmp: F64Cmp,
) -> std::result::Result<(), Vec<Error<O1::B, O2::B, O1::F, O2::F>>>
// The first value needs to be bool, so we can check for early reeturn
where
    O1: OutputValue<B = bool>,
    O2: OutputValue,
    BoolCmp: Fn(&O1::B, &O2::B) -> bool,
    // The third parameter is io.output.max_error and can be ignored if irreleveant
    F64Cmp: Fn(&O1::F, &O2::F, f64) -> bool,
    O2::B: std::fmt::Debug + Clone,
    O1::F: std::fmt::Debug + Clone,
    O2::F: std::fmt::Debug + Clone,
{
    if lhs.results.len() != rhs.results.len() {
        return Err(vec![Error::ArrayLengthMissmatch {
            reference: lhs.results.len(),
            fixedpoint: rhs.results.len(),
        }]);
    }
    let mut all_errors = vec![];
    for (i, (reference, fp)) in lhs.results.iter().zip(rhs.results.iter()).enumerate() {
        let mut saw_early_ret = false;

        let mut errors = vec![];
        let mut early_ret_errors = vec![];

        for value in &io.outputs {
            match value.kind {
                OutputKind::Boolean { early_ret } => {
                    let ref_value = reference.get(&value.name).unwrap().assume_bool();
                    let fp_value = fp.get(&value.name).unwrap().assume_bool();

                    if !bool_cmp(&ref_value, &fp_value) {
                        let err = Error::BoolMissmatch {
                            expected: ref_value,
                            got: fp_value,
                            variable: value.name.clone(),
                            input_num: i,
                        };
                        if early_ret.is_some() {
                            early_ret_errors.push(err.clone())
                        }
                        errors.push(err)
                    }

                    match (ref_value, early_ret) {
                        (true, Some(EarlyRetKind::InvalidWhenHigh)) => saw_early_ret = true,
                        (false, Some(EarlyRetKind::InvalidWhenLow)) => saw_early_ret = true,
                        _ => {}
                    }
                }
                OutputKind::Frac { max_error } => {
                    let ref_value = reference.get(&value.name).unwrap().assume_f64();
                    let fp_value = fp.get(&value.name).unwrap().assume_f64();

                    if !f64_cmp(&ref_value, &fp_value, max_error) {
                        errors.push(Error::F64Missmatch {
                            expected: ref_value,
                            got: fp_value,
                            variable: value.name.clone(),
                            input_num: i,
                        })
                    }
                }
            }
        }

        if saw_early_ret {
            if !early_ret_errors.is_empty() {
                all_errors.append(&mut early_ret_errors);
            }
        } else {
            if !errors.is_empty() {
                all_errors.append(&mut errors);
            }
        }
    }
    if all_errors.is_empty() {
        Ok(())
    } else {
        Err(all_errors)
    }
}
pub fn check_fixedpoint(
    io: &IoValues,
    references: &OutputValues<RefOutputValue>,
    fixedpoints: &OutputValues<FpOutputValue>,
) -> std::result::Result<(), Vec<Error<bool, bool, f64, (f64, i128)>>> {
    check_values(
        io,
        references,
        fixedpoints,
        |b1, b2| b1 == b2,
        |f1, f2, max_error| (f1 - f2.0).abs() < max_error,
    )
}

pub type CheckResult<B1, B2, F1, F2> = std::result::Result<(), Vec<Error<B1, B2, F1, F2>>>;

pub type VerilogCheckResult =
    CheckResult<bool, VerilogValue<bool>, (f64, i128), VerilogValue<i128>>;

pub fn check_verilog(
    io: &IoValues,
    fixedpoints: &OutputValues<FpOutputValue>,
    verilog: &OutputValues<VerilogOutputValue>,
) -> VerilogCheckResult {
    log::warn!("Not checking for 100% bit accuracy");
    check_values(
        io,
        fixedpoints,
        verilog,
        |b1, b2| b2 == b1,
        // |i1, i2, _max_error| *i2 == i1.1,
        |i1, i2, _max_error| match i2 {
            VerilogValue::Impure => false,
            VerilogValue::X => false,
            VerilogValue::Z => false,
            VerilogValue::Val(inner) => (i1.1 - inner).abs() < 128,
        }
    )
}

/// Run check_verilog on two pairs of values, printing a report and returning Ok or Err
/// depending on the result. Used for human readable output of the checks
pub fn run_checks<C, B1, B2, F1, F2, B1P, B2P, F1P, F2P>(
    io: &IoValues,
    check_name: &str,
    checker: C,
    b1p: B1P,
    b2p: B2P,
    f1p: F1P,
    f2p: F2P,
) -> Result<(), ()>
where
    C: Fn() -> CheckResult<B1, B2, F1, F2>,
    B1: Clone + std::fmt::Debug,
    B2: Clone + std::fmt::Debug,
    F1: Clone + std::fmt::Debug,
    F2: Clone + std::fmt::Debug,
    B1P: Fn(B1) -> String,
    B2P: Fn(B2) -> String,
    F1P: Fn(F1) -> String,
    F2P: Fn(F2) -> String,
{
    if let Err(errs) = checker() {
        info!("Reporting {} errors", errs.len());
        println!("{}", format!("{} missmatch:", check_name).red());
        let mut prev_input = None;
        for err in errs {
            match err {
                Error::BoolMissmatch { input_num, .. } | Error::F64Missmatch { input_num, .. } => {
                    if Some(input_num) != prev_input {
                        let inputs = io.input_blocks()[input_num]
                            .iter()
                            .map(|i| format!("{}: {}", i.0, i.1))
                            .collect::<Vec<_>>()
                            .join(", ");
                        println!("In input {} ({}): ", input_num, inputs)
                    }
                    prev_input = Some(input_num)
                }
                _ => {}
            }
            match err {
                e @ Error::ArrayLengthMissmatch { .. } => {
                    println!("\t(Internal) {}", e)
                }
                Error::BoolMissmatch {
                    expected,
                    got,
                    variable,
                    input_num: _,
                } => {
                    println!(
                        "\t{}: e: {}, g: {}",
                        format!("{}", variable).blue(),
                        format!("{}", b1p(expected)).green(),
                        format!("{}", b2p(got)).red()
                    );
                }
                Error::F64Missmatch {
                    expected,
                    got,
                    variable,
                    input_num: _,
                } => {
                    println!(
                        "\t{}: e: {}, g: {}",
                        format!("{}", variable).blue(),
                        format!("{}", f1p(expected)).green(),
                        format!("{}", f2p(got)).red()
                    );
                }
            }
        }

        Err(())
    } else {
        Ok(())
    }
}

pub fn count_valid<O>(io: &IoValues, values: &OutputValues<O>) -> usize
where
    O: OutputValue,
    O::B: PartialEq<bool>,
{
    let mut valid_count = 0;
    let mut has_seen_validity_variable = false;
    for output in &io.outputs {
        match output.kind {
            OutputKind::Boolean {
                early_ret: Some(edge),
            } => {
                if has_seen_validity_variable {
                    panic!("Multiple valid variables")
                }
                has_seen_validity_variable = true;

                let to_be_valid = match edge {
                    EarlyRetKind::InvalidWhenHigh => false,
                    EarlyRetKind::InvalidWhenLow => true,
                };

                for value in &values.results {
                    if value[&output.name].assume_bool() == to_be_valid {
                        valid_count += 1;
                    }
                }
            }
            _ => {}
        }
    }
    valid_count
}

#[cfg(test)]
mod tests {
    use std::collections::HashMap;

    use crate::config::Config;

    use super::*;

    fn default_config() -> Config {
        let config_str = r#"
        [reference]
        function = "main"
        main_file = "main.cpp"
        extra_files = []

        [fixedpoint]
        function = "main"
        main_file = "fp.cpp"
        extra_files = []

        [verilog]
        module = "uut"
        main_file = "main.v"
        extra_files = []
        pipeline_depth = 1

        [io]
        inputs = []
        outputs = [
            { name = "u1", kind = { v = "Boolean", a = {early_ret = "InvalidWhenHigh"} } },
            { name = "u2", kind = { v = "Frac", a = {max_error = 0.5} } },
            { name = "b1", kind = { v = "Boolean", a = {} } },
        ]
        "#;

        Config::from_toml(config_str).unwrap()
    }

    #[test]
    fn non_differing_fixedpoint_values_are_ok() {
        let reference = OutputValues {
            results: vec![vec![
                ("u1".to_string(), RefOutputValue::Bool(false)),
                ("u2".to_string(), RefOutputValue::F64(0.5)),
                ("b1".to_string(), RefOutputValue::Bool(false)),
            ]
            .into_iter()
            .collect::<HashMap<_, _>>()],
            internal_values: vec![],
        };
        let fixedpoint = OutputValues {
            results: vec![vec![
                ("u1".to_string(), FpOutputValue::Bool(false)),
                ("u2".to_string(), FpOutputValue::F64((0.5, 1))),
                ("b1".to_string(), FpOutputValue::Bool(false)),
            ]
            .into_iter()
            .collect::<HashMap<_, _>>()],
            internal_values: vec![],
        };

        let config = default_config();
        assert_eq!(
            check_fixedpoint(&config.io, &reference, &fixedpoint),
            Ok(())
        );
    }

    #[test]
    fn floats_within_bounds_are_ok() {
        let reference = OutputValues {
            results: vec![vec![
                ("u1".to_string(), RefOutputValue::Bool(false)),
                ("u2".to_string(), RefOutputValue::F64(0.5)),
                ("b1".to_string(), RefOutputValue::Bool(false)),
            ]
            .into_iter()
            .collect::<HashMap<_, _>>()],
            internal_values: vec![],
        };
        let fixedpoint = OutputValues {
            results: vec![vec![
                ("u1".to_string(), FpOutputValue::Bool(false)),
                ("u2".to_string(), FpOutputValue::F64((0.6, 1))),
                ("b1".to_string(), FpOutputValue::Bool(false)),
            ]
            .into_iter()
            .collect::<HashMap<_, _>>()],
            internal_values: vec![],
        };

        let config = default_config();
        assert_eq!(
            check_fixedpoint(&config.io, &reference, &fixedpoint),
            Ok(())
        );
    }

    #[test]
    fn out_of_bound_values_cause_error() {
        let reference = OutputValues {
            results: vec![
                vec![
                    ("u1".to_string(), RefOutputValue::Bool(false)),
                    ("u2".to_string(), RefOutputValue::F64(0.5)),
                    ("b1".to_string(), RefOutputValue::Bool(false)),
                ]
                .into_iter()
                .collect::<HashMap<_, _>>(),
                vec![
                    ("u1".to_string(), RefOutputValue::Bool(false)),
                    ("u2".to_string(), RefOutputValue::F64(0.5)),
                    ("b1".to_string(), RefOutputValue::Bool(false)),
                ]
                .into_iter()
                .collect::<HashMap<_, _>>(),
            ],
            internal_values: vec![],
        };
        let fixedpoint = OutputValues {
            results: vec![
                vec![
                    ("u1".to_string(), FpOutputValue::Bool(false)),
                    ("u2".to_string(), FpOutputValue::F64((0.5, 1))),
                    ("b1".to_string(), FpOutputValue::Bool(false)),
                ]
                .into_iter()
                .collect::<HashMap<_, _>>(),
                vec![
                    ("u1".to_string(), FpOutputValue::Bool(false)),
                    ("u2".to_string(), FpOutputValue::F64((1.5, 1))),
                    ("b1".to_string(), FpOutputValue::Bool(false)),
                ]
                .into_iter()
                .collect::<HashMap<_, _>>(),
            ],
            internal_values: vec![],
        };

        let config = default_config();
        assert_eq!(
            check_fixedpoint(&config.io, &reference, &fixedpoint),
            Err(vec![Error::F64Missmatch {
                expected: 0.5,
                got: (1.5, 1),
                input_num: 1,
                variable: "u2".to_string(),
            }])
        );
    }

    #[test]
    fn failure_flag_ignores_errors_in_other_values() {
        let reference = OutputValues {
            results: vec![vec![
                ("u1".to_string(), RefOutputValue::Bool(true)),
                ("u2".to_string(), RefOutputValue::F64(0.5)),
                ("b1".to_string(), RefOutputValue::Bool(false)),
            ]
            .into_iter()
            .collect::<HashMap<_, _>>()],
            internal_values: vec![],
        };
        let fixedpoint = OutputValues {
            results: vec![vec![
                ("u1".to_string(), FpOutputValue::Bool(true)),
                ("u2".to_string(), FpOutputValue::F64((1.5, 1))),
                ("b1".to_string(), FpOutputValue::Bool(false)),
            ]
            .into_iter()
            .collect::<HashMap<_, _>>()],
            internal_values: vec![],
        };

        let config = default_config();
        assert_eq!(
            check_fixedpoint(&config.io, &reference, &fixedpoint),
            Ok(())
        );
    }

    #[test]
    fn incorrect_booleans_cause_error() {
        let reference = OutputValues {
            results: vec![vec![
                ("u1".to_string(), RefOutputValue::Bool(false)),
                ("u2".to_string(), RefOutputValue::F64(0.5)),
                ("b1".to_string(), RefOutputValue::Bool(false)),
            ]
            .into_iter()
            .collect::<HashMap<_, _>>()],
            internal_values: vec![],
        };
        let fixedpoint = OutputValues {
            results: vec![vec![
                ("u1".to_string(), FpOutputValue::Bool(true)),
                ("u2".to_string(), FpOutputValue::F64((0.5, 1))),
                ("b1".to_string(), FpOutputValue::Bool(false)),
            ]
            .into_iter()
            .collect::<HashMap<_, _>>()],
            internal_values: vec![],
        };

        let config = default_config();
        assert_eq!(
            check_fixedpoint(&config.io, &reference, &fixedpoint),
            Err(vec![Error::BoolMissmatch {
                expected: false,
                got: true,
                variable: "u1".to_string(),
                input_num: 0
            }])
        );
    }

    #[test]
    fn incorrect_booleans_do_not_cause_early_ret_trimming() {
        let reference = OutputValues {
            results: vec![vec![
                ("u1".to_string(), RefOutputValue::Bool(false)),
                ("u2".to_string(), RefOutputValue::F64(0.5)),
                ("b1".to_string(), RefOutputValue::Bool(false)),
            ]
            .into_iter()
            .collect::<HashMap<_, _>>()],
            internal_values: vec![],
        };
        let fixedpoint = OutputValues {
            results: vec![vec![
                ("u1".to_string(), FpOutputValue::Bool(false)),
                ("u2".to_string(), FpOutputValue::F64((1.5, 1))),
                ("b1".to_string(), FpOutputValue::Bool(true)),
            ]
            .into_iter()
            .collect::<HashMap<_, _>>()],
            internal_values: vec![],
        };

        let config = default_config();
        let result = check_fixedpoint(&config.io, &reference, &fixedpoint);
        match result {
            Ok(_) => panic!("Expected Err, got {:?}", result),
            Err(mut e) => {
                let sorter = |e: &Error<bool, bool, f64, (f64, i128)>| match e {
                    Error::F64Missmatch { variable, .. } => variable.clone(),
                    Error::BoolMissmatch { variable, .. } => variable.clone(),
                    Error::ArrayLengthMissmatch { .. } => panic!("did not expect this error type"),
                };
                e.sort_by_key(sorter);
                let mut expected = vec![
                    Error::F64Missmatch {
                        expected: 0.5,
                        got: (1.5, 1),
                        input_num: 0,
                        variable: "u2".to_string(),
                    },
                    Error::BoolMissmatch {
                        expected: false,
                        got: true,
                        variable: "b1".to_string(),
                        input_num: 0,
                    },
                ];
                expected.sort_by_key(sorter);
                assert_eq!(e, expected)
            }
        }
    }
}
