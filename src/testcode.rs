use std::fs;

use colored::Colorize;

use crate::{run_fixedpoint_checks, run_models, run_verilog_checks, RunModelsArgs};

#[test]
fn run_output_tests() {
    let mut failed_count = 0;
    let mut success_count = 0;

    for dir in fs::read_dir("testcode/passing").unwrap() {
        match dir {
            Ok(dir) => {
                let args = RunModelsArgs {
                    test_dir: &dir.path(),
                    use_debugger: false,
                    wait_for_pipeline: false,
                    display_internal: false,
                    input_num: None,
                };

                let model_result = match run_models(args) {
                    Ok(v) => v,
                    Err(e) => panic!("{} when running {:?}", e, dir.path().to_string_lossy()),
                };

                if let Err(_) = run_fixedpoint_checks(&model_result) {
                    let message = format!("Expected {} to pass", dir.path().to_string_lossy());
                    println!("{}", message.red().bold());
                    failed_count += 1;
                } else {
                    success_count += 1;
                }
            }
            Err(_) => panic!("Dir error"),
        }
    }

    for dir in fs::read_dir("testcode/failing").unwrap() {
        match dir {
            Ok(dir) => {
                let args = RunModelsArgs {
                    test_dir: &dir.path(),
                    use_debugger: false,
                    wait_for_pipeline: false,
                    display_internal: false,
                    input_num: None,
                };

                let model_result = match run_models(args) {
                    Ok(v) => v,
                    Err(e) => panic!("{} when running {:?}", e, dir.path().to_string_lossy()),
                };

                match (
                    run_fixedpoint_checks(&model_result),
                    run_verilog_checks(&model_result),
                ) {
                    (Ok(_), Ok(_)) => {
                        let message = format!("Expected {} to fail", dir.path().to_string_lossy());
                        println!("{}", message.red().bold());
                        failed_count += 1;
                    }
                    _ => {
                        success_count += 1;
                    }
                }
            }
            Err(_) => panic!("Dir error"),
        }
    }

    if failed_count != 0 {
        panic!(
            "Output tests done. {} failures, {} successes",
            failed_count, success_count
        )
    }
}
